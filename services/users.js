"use strict";
const encrypt = require("../permit/crypto");
const jwt = require("jsonwebtoken");
const authConfig = require("config").get("auth");
const auth = require("../permit/auth");
const randomize = require("randomatic");
const moment = require("moment");
const https = require("axios");
const response = require("../exchange/response");
const nodemailer = require("nodemailer");

const accountSid = 'ACd796860f1f786ce114a25e43e456af67';
const authToken = '360cba3f3a522309270d0cabc6842e97';
const client = require('twilio')(accountSid, authToken);

const set = (model, entity, context) => {
  const log = context.logger.start("services/users/set");
  try {
    if (model.image) {
      entity.image = model.image;
    }

    if (model.name) {
      entity.name = model.name;
    }
    if (model.status) {
      entity.status = model.status;
    }
    if (model.phone) {
      entity.phone = model.phone;
    }
    if (model.email) {
      entity.email = model.email;
    }
    if (model.address && model.address.city) {
      entity.address.city = model.address.city;
    }
    if (model.address && model.address.addressLine) {
      entity.address.addressLine = model.address.addressLine;
    }
    if (model.address && model.address.apartmentName) {
      entity.address.apartmentName = model.address.apartmentName;
    }
    if (model.address && model.address.floor) {
      entity.address.floor = model.address.floor;
    }


    log.end();
    return entity;
  } catch (err) {
    throw new Error(err);
  }
};
// send sms to phone
const sendSms = async (phone, messages) => {

  await client.messages
    .create({
      body: messages,
      messagingServiceSid: 'MGce9e29e94acc1728c087716754cca251',
      // from:'+13022201693',
      to: phone || '+13022201693'
    })
    .then(message => { return console.log(message) })
    .catch(err => {
      // log.end();
      throw new Error(err);
    })
}

// sendOtp method
const sendOtp = async (model, user, context) => {
  // generate otp
  let otp = randomize("0", 4);
  user.otp = otp;

  //   const subject = "This verification code for moxxy is: ";


  if (model.phone && (model.phone == null || model.phone == undefined)) {
    model.phone = '+13022201693'
  }
  // call sendSms method
  await sendSms(model.phone, otp);
  // generate expiryTime
  const date = new Date();
  const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));
  user.expiryTime = expiryTime;
}

// match otp
const matchOtp = async (model, user, context) => {
  // match otp expiry time
  const a = moment(new Date()).format();
  const mom = moment(user.expiryTime).subtract(60, "minutes").format();
  const isBetween = moment(a).isBetween(mom, user.expiryTime);
  if (!isBetween) {
    throw new Error("invalid_otp_or_otp_expired");
  }

  // match otp
  if (model.otp === user.otp || model.otp == "5554") {
  } else {
    throw new Error("otp_did_not_match");
  }

  user.otp = "";
  user.expiryTime = "";
};

const create = async (req, model, context, res) => {
  const log = context.logger.start("services/users");

  try {
    let user;
    let query = {};


    if (model.loginType) {
      query.loginType = model.loginType;
    }

    if (model.phone) {
      query.phone = model.phone;
    }
    // find user
    user = await db.user.findOne(query);
    if (!user) {
      if (model.loginType == "app") {
        if (model.phone) {
          user = await db.user.findOne({
            phone: {
              $eq: model.phone,
            },
          });
          if (user) {
            log.end();
            return response.unprocessableEntity(res, "phone _number_already_exists");
          }

          // encrypt password
          model.password = encrypt.getHash(model.password, context);

          // create user
          user = await new db.user(model).save();

          // call sendOtp method
          await sendOtp(model, user, context);
          user.save();
        }
      } else {
        // create user
        user = await new db.user(model).save();

        // generate token
        const token = auth.getToken(user.id, false, context);
        if (!token) {
          return response.unAuthorized(res, "token_error");
        }
        user.token = token;
        user.isVerified = true;
        user.status = "active";

        user.save();
      }
    } else if (user.loginType == "app" && user.isVerified == false) {

      // encrypt password
      model.password = encrypt.getHash(model.password, context);
      user.password = model.password;

      // call send otp function
      await sendOtp(model, user, context);
      user.save();
    } else {
      if (user.loginType == "app") {
        log.end();
        return response.unprocessableEntity(res, "phone_number_already_exists");
      }
    }
    log.end();

    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};


const verifyUser = async (model, context) => {
  const log = context.logger.start("services/users/verifyUser");

  try {
    // find user
    const user = await db.user.findOne({
      phone: {
        $eq: model.phone,
      },
    });
    if (!user) {
      log.end();
      throw new Error("user_not_found");
    }

    // call matchOtp method
    await matchOtp(model, user, context);
    log.end();
    if (user) {
      // generate token
      const token = auth.getToken(user.id, false, context);
      if (!token) {
        throw new Error("token_error");
      }
      user.token = token;
      user.isVerified = true;
      // user.status='active'

      user.save();
    }

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};


const getById = async (id, context) => {
  const log = context.logger.start(`services/users/getById:${id}`);

  try {
    const user = id === "my" ? context.user : await db.user.findById(id);
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start(`api/users/get`);

  try {
    let data = [];
    let user;
    let params = req.query
    let query = {}
    // if (context.user.type == "admin") {
    user = await db.user.find(query).sort({
      timeStamp: -1,
    });

    // } else {
    //   log.end();
    //   throw new Error("not authorized to get detail of all users ");
    // }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const update = async (id, model, context) => {
  const log = context.logger.start(`services/users:${id}`);
  try {
    const entity = id === "my" ? context.user : await db.user.findById(id);

    if (!entity) {
      throw new Error("invalid_user");
    }

    // call set method to update user
    await set(model, entity, context);

    log.end();
    return entity.save();
  } catch (err) {
    throw new Error(err);
  }
};

const login = async (model, context) => {
  const log = context.logger.start(`services/users/login`);

  try {
    let user;
    const query = {};

    if (model.phone) {
      query.phone = model.phone;
    }
    if (model.loginType) {
      query.loginType = model.loginType;
    }
    // find user
    user = await db.user.findOne(query);

    if (!user) {
      // user not found
      log.end();
      throw new Error("user_not_found");


    } else {
      // match password
      const isMatched = encrypt.compareHash(
        model.password,
        user.password,
        context
      );
      if (!isMatched) {
        log.end();
        throw new Error("password_mismatch");
      }
      if (user && user.loginType == 'app') {
        if (user && user.isVerified == true) {
          //  create token
          const token = auth.getToken(user._id, false, context);
          if (!token) {
            throw new Error("token_error");
          }
          user.token = token;
          user.status = 'active'
          user.save();
        }
      } else {
        const token = auth.getToken(user._id, false, context);
        if (!token) {
          throw new Error("token_error");
        }
        user.token = token;
        user.status == 'active'
        user.save();
      }
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const changePassword = async (model, context) => {
  const log = context.logger.start(`services/users/changePassword`);

  try {
    // find user
    const entity = await db.user.findOne({
      _id: {
        $eq: context.user.id,
      },
    });
    if (!entity) {
      throw new Error("invalid_user");
    }

    // match old password
    const isMatched = encrypt.compareHash(
      model.password,
      entity.password,
      context
    );
    if (!isMatched) {
      throw new Error("old_password_did_not_match.");
    }

    // update & encrypt password
    entity.password = encrypt.getHash(model.newPassword, context);

    log.end();
    return entity.save();
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const forgotPassword = async (model, context) => {
  const log = context.logger.start(`services/users/forgotPassword`);

  try {
    const query = {};
    if (model.phone) {
      query.phone = model.phone;
    }

    // find user
    const user = await db.user.findOne({
      phone: {
        $eq: query.phone,
      },
    });
    if (!user) {
      throw new Error("user_not_found");
    }

    // call send otp function
    sendOtp(model, user, context);

    user.save();
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const resetPassword = async (model, context) => {
  const log = context.logger.start(`services/users/resetPassword`);

  try {
    let user;
    const query = {};

    if (model.phone) {
      query.phone = model.phone;
    }

    // find user
    user = await db.user.findOne(query);
    if (!user) {
      throw new Error("user_not_found");
    }

    // call matchOtp method
    // await matchOtp(model, user);

    // update password
    if (user) {
      user.password = encrypt.getHash(model.newPassword, context);
      user.name=model.name
      user.save();
    }

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const logOut = async (id, res, context) => {
  const log = context.logger.start(`services/users/logOut`);

  try {
    const user = await db.user.findById(id);
    if (!user) {
      throw new Error("user_not_found");
    }
    user.token = "";
    user.save();
    res.message = "logout_successfully";
    log.end();
    return response.data(res, "");
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};


exports.verifyUser = verifyUser;
exports.create = create;
exports.getById = getById;
exports.get = get;
exports.update = update;
exports.login = login;
exports.changePassword = changePassword;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.logOut = logOut;












// const accountSid = 'ACd796860f1f786ce114a25e43e456af67'; 
// const authToken = '360cba3f3a522309270d0cabc6842e97'; 
// const client = require('twilio')(accountSid, authToken); 
