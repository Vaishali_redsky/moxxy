'use strict'

// users create mapper
exports.toModel = entity => {

    var model = {
        id: entity._id,

        name: entity.name, 
        type: entity.type,
        email: entity.email,
        phone: entity.phone,
        status: entity.status,
        // image:entity.image,
        isVerified: entity.isVerified,
        // otp:entity.otp,
        
        address: entity.address,
        loginType:entity.loginType


    }

    
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        id: entity._id,

        name: entity.name,
        email: entity.email,
        phone: entity.phone,
        isVerified: entity.isVerified,
        image:entity.image,
        address: entity.address,
        // loginType:entity.loginType,
        // token: entity.token,
        
    }
   
    return model

}

// for login 
exports.toUser = entity => {
    var model = {
        id: entity._id,

        name: entity.name,
        email: entity.email,
        phone: entity.phone,
        isVerified: entity.isVerified,
        image:entity.image,
        address: entity.address,
        // loginType:entity.loginType,
        token: entity.token,
        
    }
   
    return model
}

// for get 

exports.toSearchModel = (entities, context) => {
    return entities.map((entity) => {
        return exports.toModel(entity, context)
    })
}