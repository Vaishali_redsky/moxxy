module.exports = {

    name: 'string',
    phone: 'string',
    email: 'string',
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },

    status: 'string',

    address: {
        city: 'string',
        addressLine: 'string',
        apartmentName: 'string',
        floor: 'string'
    },


}

