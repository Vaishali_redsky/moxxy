'use strict'

// User Module
module.exports = {
    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin','seller']
    },
    
    loginType: {
        type: String,
        default: 'app',
        enum: ['app', 'google', 'facebook']
    },
    status: {
        type: String,
        default: 'inactive',
        enum: ['active', 'inactive']
    },
    phone: {
        type: String,
        unique: false,
    },
    name:String,
    email:String,
    password: String,
    // address: {
    //     city:String,
    //     addressLine:String,
    //     apartmentName: String,
    //     floor:String
    // },
    image:{
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    },
    expiryTime: String,
    otp: String,
    isVerified:{
        type:Boolean,
        default:false
    },
    // qrCode:String,
    token: String, 
    // uId: String,
    tempToken: String,
   

}



